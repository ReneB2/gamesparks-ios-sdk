
#import "GSData.h"

#ifndef GameSparks_IOS_GSAPI_h
#define GameSparks_IOS_GSAPI_h

@interface GSUploadData : GSData
-(NSString*) getFileName;
-(NSString*) getPlayerId;
@end
@interface GSSocialStatus : GSData
-(BOOL) getActive;
-(NSDate*) getExpires;
-(NSString*) getSystemId;
@end
@interface GSInvitableFriend : GSData
-(NSString*) getDisplayName;
-(NSString*) getId;
-(NSString*) getProfilePic;
@end
@interface GSPlayer : GSData
		
-(NSString*) getAchievements;
-(NSString*) getDisplayName;
-(NSDictionary*) getExternalIds;
-(NSString*) getId;
-(BOOL) getOnline;
-(NSDictionary*) getScriptData;
		
-(NSString*) getVirtualGoods;
@end
@interface GSParticipant : GSData
		
-(NSString*) getAchievements;
-(NSString*) getDisplayName;
-(NSDictionary*) getExternalIds;
-(NSString*) getId;
-(BOOL) getOnline;
-(NSNumber*) getPeerId;
-(NSDictionary*) getScriptData;
		
-(NSString*) getVirtualGoods;
@end
@interface GSPlayerDetail : GSData
-(NSDictionary*) getExternalIds;
-(NSString*) getId;
-(NSString*) getName;
@end
@interface GSLocation : GSData
-(NSString*) getCity;
-(NSString*) getCountry;
-(NSNumber*) getLatitide;
-(NSNumber*) getLongditute;
@end
@interface GSTeam : GSData
-(NSArray*) getMembers;
-(GSPlayer*) getOwner;
-(NSString*) getTeamId;
-(NSString*) getTeamType;
@end
@interface GSPlayerTurnCount : GSData
-(NSString*) getCount;
-(NSString*) getPlayerId;
@end
@interface GSLeaderboard : GSData
-(NSString*) getDescription;
-(NSString*) getName;
-(NSDictionary*) getPropertySet;
-(NSString*) getShortCode;
@end
@interface GSLeaderboardData : GSData
-(NSString*) getCity;
-(NSString*) getCountry;
-(NSDictionary*) getExternalIds;
-(NSString*) getId;
-(NSNumber*) getRank;
-(NSString*) getUserId;
-(NSString*) getUserName;
-(NSString*) getWhen;
-(NSNumber*)getNumberValue:(NSString*)shortCode;
-(NSString*)getStringValue:(NSString*)shortCode;
@end
@interface GSLeaderboardRankDetails : GSData
-(NSArray*) getFriendsPassed;
-(NSNumber*) getGlobalCount;
-(NSNumber*) getGlobalFrom;
-(NSNumber*) getGlobalFromPercent;
-(NSNumber*) getGlobalTo;
-(NSNumber*) getGlobalToPercent;
-(NSNumber*) getSocialCount;
-(NSNumber*) getSocialFrom;
-(NSNumber*) getSocialFromPercent;
-(NSNumber*) getSocialTo;
-(NSNumber*) getSocialToPercent;
-(NSArray*) getTopNPassed;
@end
@interface GSChallenge : GSData
-(NSArray*) getAccepted;
-(NSString*) getChallengeId;
-(NSString*) getChallengeMessage;
-(NSString*) getChallengeName;
-(NSArray*) getChallenged;
-(GSPlayerDetail*) getChallenger;
-(NSNumber*) getCurrency1Wager;
-(NSNumber*) getCurrency2Wager;
-(NSNumber*) getCurrency3Wager;
-(NSNumber*) getCurrency4Wager;
-(NSNumber*) getCurrency5Wager;
-(NSNumber*) getCurrency6Wager;
-(NSArray*) getDeclined;
-(NSDate*) getEndDate;
-(NSDate*) getExpiryDate;
-(NSNumber*) getMaxTurns;
-(NSString*) getNextPlayer;
-(NSDictionary*) getScriptData;
-(NSString*) getShortCode;
-(NSDate*) getStartDate;
-(NSString*) getState;
-(NSArray*) getTurnCount;
@end
@interface GSBoughtitem : GSData
-(NSNumber*) getQuantity;
-(NSString*) getShortCode;
@end
@interface GSVirtualGood : GSData
-(NSString*) getWP8StoreProductId;
-(NSString*) getAmazonStoreProductId;
-(NSNumber*) getCurrency1Cost;
-(NSNumber*) getCurrency2Cost;
-(NSNumber*) getCurrency3Cost;
-(NSNumber*) getCurrency4Cost;
-(NSNumber*) getCurrency5Cost;
-(NSNumber*) getCurrency6Cost;
-(NSString*) getDescription;
-(NSString*) getGooglePlayProductId;
-(NSString*) getIosAppStoreProductId;
-(NSNumber*) getMaxQuantity;
-(NSString*) getName;
-(NSDictionary*) getPropertySet;
-(NSString*) getShortCode;
-(NSString*) getTags;
-(NSString*) getType;
-(NSString*) getW8StoreProductId;
@end
@interface GSAchievement : GSData
-(NSString*) getDescription;
-(BOOL) getEarned;
-(NSString*) getName;
-(NSDictionary*) getPropertySet;
-(NSString*) getShortCode;
@end
@interface GSChallengeType : GSData
-(NSString*) getChallengeShortCode;
-(NSString*) getDescription;
-(NSString*) getGetleaderboardName;
-(NSString*) getName;
-(NSString*) getTags;
@end

@interface GSAcceptChallengeResponse : GSResponse
-(NSString*) getChallengeInstanceId;
@end
@interface GSAccountDetailsResponse : GSResponse
		
-(NSString*) getAchievements;
-(NSNumber*) getCurrency1;
-(NSNumber*) getCurrency2;
-(NSNumber*) getCurrency3;
-(NSNumber*) getCurrency4;
-(NSNumber*) getCurrency5;
-(NSNumber*) getCurrency6;
-(NSString*) getDisplayName;
-(NSDictionary*) getExternalIds;
-(GSLocation*) getLocation;
-(NSDictionary*) getReservedCurrency1;
-(NSDictionary*) getReservedCurrency2;
-(NSDictionary*) getReservedCurrency3;
-(NSDictionary*) getReservedCurrency4;
-(NSDictionary*) getReservedCurrency5;
-(NSDictionary*) getReservedCurrency6;
-(NSString*) getUserId;
-(NSDictionary*) getVirtualGoods;
@end
@interface GSAnalyticsResponse : GSResponse
@end
@interface GSAroundMeLeaderboardResponse : GSResponse
-(NSString*) getChallengeInstanceId;
-(NSArray*) getData;
-(NSArray*) getFirst;
-(NSArray*) getLast;
-(NSString*) getLeaderboardShortCode;
-(BOOL) getSocial;
@end
@interface GSAuthenticationResponse : GSResponse
-(NSString*) getAuthToken;
-(NSString*) getDisplayName;
-(BOOL) getNewPlayer;
-(GSPlayer*) getSwitchSummary;
-(NSString*) getUserId;
@end
@interface GSBuyVirtualGoodResponse : GSResponse
-(NSArray*) getBoughtItems;
-(NSNumber*) getCurrency1Added;
-(NSNumber*) getCurrency2Added;
-(NSNumber*) getCurrency3Added;
-(NSNumber*) getCurrency4Added;
-(NSNumber*) getCurrency5Added;
-(NSNumber*) getCurrency6Added;
-(NSNumber*) getCurrencyConsumed;
-(NSNumber*) getCurrencyType;
@end
@interface GSChangeUserDetailsResponse : GSResponse
@end
@interface GSChatOnChallengeResponse : GSResponse
-(NSString*) getChallengeInstanceId;
@end
@interface GSConsumeVirtualGoodResponse : GSResponse
@end
@interface GSCreateChallengeResponse : GSResponse
-(NSString*) getChallengeInstanceId;
@end
@interface GSCreateTeamResponse : GSResponse
-(NSArray*) getMembers;
-(GSPlayer*) getOwner;
-(NSString*) getTeamId;
-(NSString*) getTeamType;
@end
@interface GSDeclineChallengeResponse : GSResponse
-(NSString*) getChallengeInstanceId;
@end
@interface GSDismissMessageResponse : GSResponse
@end
@interface GSDropTeamResponse : GSResponse
-(NSArray*) getMembers;
-(GSPlayer*) getOwner;
-(NSString*) getTeamId;
-(NSString*) getTeamType;
@end
@interface GSEndSessionResponse : GSResponse
@end
@interface GSFindChallengeResponse : GSResponse
-(NSArray*) getChallengeInstances;
@end
@interface GSFindMatchResponse : GSResponse
-(NSString*) getAccessToken;
-(NSString*) getHost;
-(NSString*) getMatchId;
-(NSArray*) getOpponents;
-(NSNumber*) getPeerId;
-(NSString*) getPlayerId;
-(NSNumber*) getPort;
@end
@interface GSGameSparksErrorResponse : GSResponse
@end
@interface GSGetChallengeResponse : GSResponse
-(GSChallenge*) getChallenge;
@end
@interface GSGetDownloadableResponse : GSResponse
-(NSDate*) getLastModified;
-(NSString*) getShortCode;
-(NSNumber*) getSize;
-(NSString*) getUrl;
@end
@interface GSGetLeaderboardEntriesResponse : GSResponse
-(NSDictionary*) getResults;
-(GSLeaderboardData*)getEntryForLeaderboard:(NSString*)shortCode;
@end
@interface GSGetMessageResponse : GSResponse
-(NSDictionary*) getMessage;
@end
@interface GSGetMyTeamsResponse : GSResponse
-(NSArray*) getTeams;
@end
@interface GSGetPropertyResponse : GSResponse
-(NSDictionary*) getProperty;
@end
@interface GSGetPropertySetResponse : GSResponse
-(NSDictionary*) getPropertySet;
@end
@interface GSGetRunningTotalsResponse : GSResponse
-(NSDictionary*) getRunningTotals;
@end
@interface GSGetTeamResponse : GSResponse
-(NSArray*) getMembers;
-(GSPlayer*) getOwner;
-(NSString*) getTeamId;
-(NSString*) getTeamType;
-(NSArray*) getTeams;
@end
@interface GSGetUploadUrlResponse : GSResponse
-(NSString*) getUrl;
@end
@interface GSGetUploadedResponse : GSResponse
-(NSNumber*) getSize;
-(NSString*) getUrl;
@end
@interface GSJoinChallengeResponse : GSResponse
@end
@interface GSJoinTeamResponse : GSResponse
-(NSArray*) getMembers;
-(GSPlayer*) getOwner;
-(NSString*) getTeamId;
-(NSString*) getTeamType;
@end
@interface GSLeaderboardDataResponse : GSResponse
-(NSString*) getChallengeInstanceId;
-(NSArray*) getData;
-(NSArray*) getFirst;
-(NSArray*) getLast;
-(NSString*) getLeaderboardShortCode;
@end
@interface GSLeaveTeamResponse : GSResponse
-(NSArray*) getMembers;
-(GSPlayer*) getOwner;
-(NSString*) getTeamId;
-(NSString*) getTeamType;
@end
@interface GSListAchievementsResponse : GSResponse
-(NSArray*) getAchievements;
@end
@interface GSListChallengeResponse : GSResponse
-(NSArray*) getChallengeInstances;
@end
@interface GSListChallengeTypeResponse : GSResponse
-(NSArray*) getChallengeTemplates;
@end
@interface GSListGameFriendsResponse : GSResponse
-(NSArray*) getFriends;
@end
@interface GSListInviteFriendsResponse : GSResponse
-(NSArray*) getFriends;
@end
@interface GSListLeaderboardsResponse : GSResponse
-(NSArray*) getLeaderboards;
@end
@interface GSListMessageResponse : GSResponse
		
-(NSDictionary*) getMessageList;
@end
@interface GSListMessageSummaryResponse : GSResponse
		
-(NSDictionary*) getMessageList;
@end
@interface GSListTeamChatResponse : GSResponse
@end
@interface GSListVirtualGoodsResponse : GSResponse
-(NSArray*) getVirtualGoods;
@end
@interface GSLogChallengeEventResponse : GSResponse
@end
@interface GSLogEventResponse : GSResponse
@end
@interface GSMatchDetailsResponse : GSResponse
-(NSString*) getAccessToken;
-(NSString*) getHost;
-(NSString*) getMatchId;
-(NSArray*) getOpponents;
-(NSNumber*) getPeerId;
-(NSString*) getPlayerId;
-(NSNumber*) getPort;
@end
@interface GSMatchmakingResponse : GSResponse
@end
@interface GSPushRegistrationResponse : GSResponse
-(NSString*) getRegistrationId;
@end
@interface GSRegistrationResponse : GSResponse
-(NSString*) getAuthToken;
-(NSString*) getDisplayName;
-(BOOL) getNewPlayer;
-(GSPlayer*) getSwitchSummary;
-(NSString*) getUserId;
@end
@interface GSSendFriendMessageResponse : GSResponse
@end
@interface GSSendTeamChatMessageResponse : GSResponse
@end
@interface GSSocialDisconnectResponse : GSResponse
@end
@interface GSSocialStatusResponse : GSResponse
-(NSArray*) getStatuses;
@end
@interface GSWithdrawChallengeResponse : GSResponse
-(NSString*) getChallengeInstanceId;
@end

@interface GSAchievementEarnedMessage : GSMessage
-(NSString*) getAchievementName;
-(NSString*) getAchievementShortCode;
-(NSString*) getCurrency1Earned;
-(NSString*) getCurrency2Earned;
-(NSString*) getCurrency3Earned;
-(NSString*) getCurrency4Earned;
-(NSString*) getCurrency5Earned;
-(NSString*) getCurrency6Earned;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSString*) getVirtualGoodEarned;
@end
@interface GSChallengeAcceptedMessage : GSMessage
-(GSChallenge*) getChallenge;
-(NSString*) getMessage;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSString*) getWho;
@end
@interface GSChallengeChangedMessage : GSMessage
-(GSChallenge*) getChallenge;
-(GSLeaderboardData*) getLeaderboardData;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSString*) getWho;
@end
@interface GSChallengeChatMessage : GSMessage
-(GSChallenge*) getChallenge;
-(NSString*) getMessage;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSString*) getWho;
@end
@interface GSChallengeDeclinedMessage : GSMessage
-(GSChallenge*) getChallenge;
-(NSString*) getMessage;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSString*) getWho;
@end
@interface GSChallengeDrawnMessage : GSMessage
-(GSChallenge*) getChallenge;
-(GSLeaderboardData*) getLeaderboardData;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
@end
@interface GSChallengeExpiredMessage : GSMessage
-(GSChallenge*) getChallenge;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
@end
@interface GSChallengeIssuedMessage : GSMessage
-(GSChallenge*) getChallenge;
-(NSString*) getMessage;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSString*) getWho;
@end
@interface GSChallengeJoinedMessage : GSMessage
-(GSChallenge*) getChallenge;
-(NSString*) getMessage;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSString*) getWho;
@end
@interface GSChallengeLapsedMessage : GSMessage
-(GSChallenge*) getChallenge;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
@end
@interface GSChallengeLostMessage : GSMessage
-(GSChallenge*) getChallenge;
-(GSLeaderboardData*) getLeaderboardData;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSString*) getWinnerName;
@end
@interface GSChallengeStartedMessage : GSMessage
-(GSChallenge*) getChallenge;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
@end
@interface GSChallengeTurnTakenMessage : GSMessage
-(GSChallenge*) getChallenge;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSString*) getWho;
@end
@interface GSChallengeWaitingMessage : GSMessage
-(GSChallenge*) getChallenge;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
@end
@interface GSChallengeWithdrawnMessage : GSMessage
-(GSChallenge*) getChallenge;
-(NSString*) getMessage;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSString*) getWho;
@end
@interface GSChallengeWonMessage : GSMessage
-(GSChallenge*) getChallenge;
-(NSNumber*) getCurrency1Won;
-(NSNumber*) getCurrency2Won;
-(NSNumber*) getCurrency3Won;
-(NSNumber*) getCurrency4Won;
-(NSNumber*) getCurrency5Won;
-(NSNumber*) getCurrency6Won;
-(GSLeaderboardData*) getLeaderboardData;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSString*) getWinnerName;
@end
@interface GSFriendMessage : GSMessage
-(NSString*) getFromId;
-(NSString*) getMessage;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSString*) getWho;
@end
@interface GSGlobalRankChangedMessage : GSMessage
-(NSNumber*) getGameId;
-(NSString*) getLeaderboardName;
-(NSString*) getLeaderboardShortCode;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(GSLeaderboardData*) getThem;
-(NSString*) getTitle;
-(GSLeaderboardData*) getYou;
@end
@interface GSMatchFoundMessage : GSMessage
-(NSString*) getAccessToken;
-(NSString*) getHost;
-(NSString*) getMatchGroup;
-(NSString*) getMatchId;
-(NSString*) getMatchShortCode;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSArray*) getParticipants;
-(NSNumber*) getPort;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
@end
@interface GSMatchNotFoundMessage : GSMessage
-(NSString*) getMatchGroup;
-(NSString*) getMatchShortCode;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSArray*) getParticipants;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
@end
@interface GSMatchUpdatedMessage : GSMessage
-(NSString*) getMatchGroup;
-(NSString*) getMatchShortCode;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSArray*) getParticipants;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
@end
@interface GSNewHighScoreMessage : GSMessage
-(GSLeaderboardData*) getLeaderboardData;
-(NSString*) getLeaderboardName;
-(NSString*) getLeaderboardShortCode;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(GSLeaderboardRankDetails*) getRankDetails;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
@end
@interface GSNewTeamScoreMessage : GSMessage
-(GSLeaderboardData*) getLeaderboardData;
-(NSString*) getLeaderboardName;
-(NSString*) getLeaderboardShortCode;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(GSLeaderboardRankDetails*) getRankDetails;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
@end
@interface GSScriptMessage : GSMessage
-(NSDictionary*) getData;
-(NSString*) getExtCode;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
@end
@interface GSSessionTerminatedMessage : GSMessage
-(NSString*) getAuthToken;
@end
@interface GSSocialRankChangedMessage : GSMessage
-(NSNumber*) getGameId;
-(NSString*) getLeaderboardName;
-(NSString*) getLeaderboardShortCode;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(GSLeaderboardData*) getThem;
-(NSString*) getTitle;
-(GSLeaderboardData*) getYou;
@end
@interface GSTeamChatMessage : GSMessage
-(NSString*) getChatMessageId;
-(NSString*) getFromId;
-(NSString*) getMessage;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getOwnerId;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTeamId;
-(NSString*) getTeamType;
-(NSString*) getTitle;
-(NSString*) getWho;
@end
@interface GSTeamRankChangedMessage : GSMessage
-(NSNumber*) getGameId;
-(NSString*) getLeaderboardName;
-(NSString*) getLeaderboardShortCode;
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(GSLeaderboardData*) getThem;
-(NSString*) getTitle;
-(GSLeaderboardData*) getYou;
@end
@interface GSUploadCompleteMessage : GSMessage
-(NSString*) getMessageId;
-(BOOL) getNotification;
-(NSString*) getSubTitle;
-(NSString*) getSummary;
-(NSString*) getTitle;
-(NSDictionary*) getUploadData;
-(NSString*) getUploadId;
@end


@interface GSAcceptChallengeRequest : GSRequest
-(void) setCallback:(void (^)(GSAcceptChallengeResponse*))callback;
-(void) setChallengeInstanceId:(NSString*) challengeInstanceId;
-(void) setMessage:(NSString*) message;
@end


@interface GSAccountDetailsRequest : GSRequest
-(void) setCallback:(void (^)(GSAccountDetailsResponse*))callback;
@end


@interface GSAmazonBuyGoodsRequest : GSRequest
-(void) setCallback:(void (^)(GSBuyVirtualGoodResponse*))callback;
-(void) setAmazonUserId:(NSString*) amazonUserId;
-(void) setReceiptId:(NSString*) receiptId;
-(void) setUniqueTransactionByPlayer:(BOOL) uniqueTransactionByPlayer;
@end


@interface GSAmazonConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setAccessToken:(NSString*) accessToken;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setSegments:(NSDictionary*) segments;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
@end


@interface GSAnalyticsRequest : GSRequest
-(void) setCallback:(void (^)(GSAnalyticsResponse*))callback;
-(void) setData:(NSDictionary*) data;
-(void) setEnd:(BOOL) end;
-(void) setKey:(NSString*) key;
-(void) setStart:(BOOL) start;
@end


@interface GSAroundMeLeaderboardRequest : GSRequest
-(void) setCallback:(void (^)(GSAroundMeLeaderboardResponse*))callback;
-(void) setChallengeInstanceId:(NSString*) challengeInstanceId;
-(void) setDontErrorOnNotSocial:(BOOL) dontErrorOnNotSocial;
-(void) setEntryCount:(NSNumber*) entryCount;
-(void) setFriendIds:(NSArray*) friendIds;
-(void) setIncludeFirst:(NSNumber*) includeFirst;
-(void) setIncludeLast:(NSNumber*) includeLast;
-(void) setInverseSocial:(BOOL) inverseSocial;
-(void) setLeaderboardShortCode:(NSString*) leaderboardShortCode;
-(void) setSocial:(BOOL) social;
-(void) setTeamIds:(NSArray*) teamIds;
-(void) setTeamTypes:(NSArray*) teamTypes;
@end


@interface GSAuthenticationRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setPassword:(NSString*) password;
-(void) setUserName:(NSString*) userName;
@end


@interface GSBuyVirtualGoodsRequest : GSRequest
-(void) setCallback:(void (^)(GSBuyVirtualGoodResponse*))callback;
-(void) setCurrencyType:(NSNumber*) currencyType;
-(void) setQuantity:(NSNumber*) quantity;
-(void) setShortCode:(NSString*) shortCode;
@end


@interface GSChangeUserDetailsRequest : GSRequest
-(void) setCallback:(void (^)(GSChangeUserDetailsResponse*))callback;
-(void) setDisplayName:(NSString*) displayName;
-(void) setLanguage:(NSString*) language;
-(void) setNewPassword:(NSString*) newPassword;
-(void) setOldPassword:(NSString*) oldPassword;
-(void) setUserName:(NSString*) userName;
@end


@interface GSChatOnChallengeRequest : GSRequest
-(void) setCallback:(void (^)(GSChatOnChallengeResponse*))callback;
-(void) setChallengeInstanceId:(NSString*) challengeInstanceId;
-(void) setMessage:(NSString*) message;
@end


@interface GSConsumeVirtualGoodRequest : GSRequest
-(void) setCallback:(void (^)(GSConsumeVirtualGoodResponse*))callback;
-(void) setQuantity:(NSNumber*) quantity;
-(void) setShortCode:(NSString*) shortCode;
@end


@interface GSCreateChallengeRequest : GSRequest
-(void) setCallback:(void (^)(GSCreateChallengeResponse*))callback;
-(void) setAccessType:(NSString*) accessType;
-(void) setAutoStartJoinedChallengeOnMaxPlayers:(BOOL) autoStartJoinedChallengeOnMaxPlayers;
-(void) setChallengeMessage:(NSString*) challengeMessage;
-(void) setChallengeShortCode:(NSString*) challengeShortCode;
-(void) setCurrency1Wager:(NSNumber*) currency1Wager;
-(void) setCurrency2Wager:(NSNumber*) currency2Wager;
-(void) setCurrency3Wager:(NSNumber*) currency3Wager;
-(void) setCurrency4Wager:(NSNumber*) currency4Wager;
-(void) setCurrency5Wager:(NSNumber*) currency5Wager;
-(void) setCurrency6Wager:(NSNumber*) currency6Wager;
-(void) setEligibilityCriteria:(NSDictionary*) eligibilityCriteria;
-(void) setEndTime:(NSDate*) endTime;
-(void) setExpiryTime:(NSDate*) expiryTime;
-(void) setMaxAttempts:(NSNumber*) maxAttempts;
-(void) setMaxPlayers:(NSNumber*) maxPlayers;
-(void) setMinPlayers:(NSNumber*) minPlayers;
-(void) setSilent:(BOOL) silent;
-(void) setStartTime:(NSDate*) startTime;
-(void) setUsersToChallenge:(NSArray*) usersToChallenge;
@end


@interface GSCreateTeamRequest : GSRequest
-(void) setCallback:(void (^)(GSCreateTeamResponse*))callback;
-(void) setTeamId:(NSString*) teamId;
-(void) setTeamName:(NSString*) teamName;
-(void) setTeamType:(NSString*) teamType;
@end


@interface GSDeclineChallengeRequest : GSRequest
-(void) setCallback:(void (^)(GSDeclineChallengeResponse*))callback;
-(void) setChallengeInstanceId:(NSString*) challengeInstanceId;
-(void) setMessage:(NSString*) message;
@end


@interface GSDeviceAuthenticationRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setDeviceId:(NSString*) deviceId;
-(void) setDeviceModel:(NSString*) deviceModel;
-(void) setDeviceName:(NSString*) deviceName;
-(void) setDeviceOS:(NSString*) deviceOS;
-(void) setDeviceType:(NSString*) deviceType;
-(void) setDisplayName:(NSString*) displayName;
-(void) setOperatingSystem:(NSString*) operatingSystem;
-(void) setSegments:(NSDictionary*) segments;
@end


@interface GSDismissMessageRequest : GSRequest
-(void) setCallback:(void (^)(GSDismissMessageResponse*))callback;
-(void) setMessageId:(NSString*) messageId;
@end


@interface GSDropTeamRequest : GSRequest
-(void) setCallback:(void (^)(GSDropTeamResponse*))callback;
-(void) setOwnerId:(NSString*) ownerId;
-(void) setTeamId:(NSString*) teamId;
-(void) setTeamType:(NSString*) teamType;
@end


@interface GSEndSessionRequest : GSRequest
-(void) setCallback:(void (^)(GSEndSessionResponse*))callback;
@end


@interface GSFacebookConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setAccessToken:(NSString*) accessToken;
-(void) setCode:(NSString*) code;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setSegments:(NSDictionary*) segments;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
@end


@interface GSFindChallengeRequest : GSRequest
-(void) setCallback:(void (^)(GSFindChallengeResponse*))callback;
-(void) setAccessType:(NSString*) accessType;
-(void) setCount:(NSNumber*) count;
-(void) setEligibility:(NSDictionary*) eligibility;
-(void) setOffset:(NSNumber*) offset;
-(void) setShortCode:(NSArray*) shortCode;
@end


@interface GSFindMatchRequest : GSRequest
-(void) setCallback:(void (^)(GSFindMatchResponse*))callback;
-(void) setAction:(NSString*) action;
-(void) setMatchGroup:(NSString*) matchGroup;
-(void) setMatchShortCode:(NSString*) matchShortCode;
-(void) setSkill:(NSNumber*) skill;
@end


@interface GSGameCenterConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setDisplayName:(NSString*) displayName;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setExternalPlayerId:(NSString*) externalPlayerId;
-(void) setPublicKeyUrl:(NSString*) publicKeyUrl;
-(void) setSalt:(NSString*) salt;
-(void) setSegments:(NSDictionary*) segments;
-(void) setSignature:(NSString*) signature;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
-(void) setTimestamp:(NSNumber*) timestamp;
@end


@interface GSGetChallengeRequest : GSRequest
-(void) setCallback:(void (^)(GSGetChallengeResponse*))callback;
-(void) setChallengeInstanceId:(NSString*) challengeInstanceId;
-(void) setMessage:(NSString*) message;
@end


@interface GSGetDownloadableRequest : GSRequest
-(void) setCallback:(void (^)(GSGetDownloadableResponse*))callback;
-(void) setShortCode:(NSString*) shortCode;
@end


@interface GSGetLeaderboardEntriesRequest : GSRequest
-(void) setCallback:(void (^)(GSGetLeaderboardEntriesResponse*))callback;
-(void) setChallenges:(NSArray*) challenges;
-(void) setInverseSocial:(BOOL) inverseSocial;
-(void) setLeaderboards:(NSArray*) leaderboards;
-(void) setPlayer:(NSString*) player;
-(void) setSocial:(BOOL) social;
-(void) setTeamTypes:(NSArray*) teamTypes;
@end


@interface GSGetMessageRequest : GSRequest
-(void) setCallback:(void (^)(GSGetMessageResponse*))callback;
-(void) setMessageId:(NSString*) messageId;
@end


@interface GSGetMyTeamsRequest : GSRequest
-(void) setCallback:(void (^)(GSGetMyTeamsResponse*))callback;
-(void) setOwnedOnly:(BOOL) ownedOnly;
-(void) setTeamTypes:(NSArray*) teamTypes;
@end


@interface GSGetPropertyRequest : GSRequest
-(void) setCallback:(void (^)(GSGetPropertyResponse*))callback;
-(void) setPropertyShortCode:(NSString*) propertyShortCode;
@end


@interface GSGetPropertySetRequest : GSRequest
-(void) setCallback:(void (^)(GSGetPropertySetResponse*))callback;
-(void) setPropertySetShortCode:(NSString*) propertySetShortCode;
@end


@interface GSGetRunningTotalsRequest : GSRequest
-(void) setCallback:(void (^)(GSGetRunningTotalsResponse*))callback;
-(void) setFriendIds:(NSArray*) friendIds;
-(void) setShortCode:(NSString*) shortCode;
@end


@interface GSGetTeamRequest : GSRequest
-(void) setCallback:(void (^)(GSGetTeamResponse*))callback;
-(void) setOwnerId:(NSString*) ownerId;
-(void) setTeamId:(NSString*) teamId;
-(void) setTeamType:(NSString*) teamType;
@end


@interface GSGetUploadUrlRequest : GSRequest
-(void) setCallback:(void (^)(GSGetUploadUrlResponse*))callback;
-(void) setUploadData:(NSDictionary*) uploadData;
@end


@interface GSGetUploadedRequest : GSRequest
-(void) setCallback:(void (^)(GSGetUploadedResponse*))callback;
-(void) setUploadId:(NSString*) uploadId;
@end


@interface GSGooglePlayBuyGoodsRequest : GSRequest
-(void) setCallback:(void (^)(GSBuyVirtualGoodResponse*))callback;
-(void) setSignature:(NSString*) signature;
-(void) setSignedData:(NSString*) signedData;
-(void) setUniqueTransactionByPlayer:(BOOL) uniqueTransactionByPlayer;
@end


@interface GSGooglePlusConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setAccessToken:(NSString*) accessToken;
-(void) setCode:(NSString*) code;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setRedirectUri:(NSString*) redirectUri;
-(void) setSegments:(NSDictionary*) segments;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
@end


@interface GSIOSBuyGoodsRequest : GSRequest
-(void) setCallback:(void (^)(GSBuyVirtualGoodResponse*))callback;
-(void) setReceipt:(NSString*) receipt;
-(void) setSandbox:(BOOL) sandbox;
-(void) setUniqueTransactionByPlayer:(BOOL) uniqueTransactionByPlayer;
@end


@interface GSJoinChallengeRequest : GSRequest
-(void) setCallback:(void (^)(GSJoinChallengeResponse*))callback;
-(void) setChallengeInstanceId:(NSString*) challengeInstanceId;
-(void) setEligibility:(NSDictionary*) eligibility;
-(void) setMessage:(NSString*) message;
@end


@interface GSJoinTeamRequest : GSRequest
-(void) setCallback:(void (^)(GSJoinTeamResponse*))callback;
-(void) setOwnerId:(NSString*) ownerId;
-(void) setTeamId:(NSString*) teamId;
-(void) setTeamType:(NSString*) teamType;
@end


@interface GSKongregateConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setGameAuthToken:(NSString*) gameAuthToken;
-(void) setSegments:(NSDictionary*) segments;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
-(void) setUserId:(NSString*) userId;
@end


@interface GSLeaderboardDataRequest : GSRequest
-(void) setCallback:(void (^)(GSLeaderboardDataResponse*))callback;
-(void) setChallengeInstanceId:(NSString*) challengeInstanceId;
-(void) setDontErrorOnNotSocial:(BOOL) dontErrorOnNotSocial;
-(void) setEntryCount:(NSNumber*) entryCount;
-(void) setFriendIds:(NSArray*) friendIds;
-(void) setIncludeFirst:(NSNumber*) includeFirst;
-(void) setIncludeLast:(NSNumber*) includeLast;
-(void) setInverseSocial:(BOOL) inverseSocial;
-(void) setLeaderboardShortCode:(NSString*) leaderboardShortCode;
-(void) setOffset:(NSNumber*) offset;
-(void) setSocial:(BOOL) social;
-(void) setTeamIds:(NSArray*) teamIds;
-(void) setTeamTypes:(NSArray*) teamTypes;
@end


@interface GSLeaveTeamRequest : GSRequest
-(void) setCallback:(void (^)(GSLeaveTeamResponse*))callback;
-(void) setOwnerId:(NSString*) ownerId;
-(void) setTeamId:(NSString*) teamId;
-(void) setTeamType:(NSString*) teamType;
@end


@interface GSListAchievementsRequest : GSRequest
-(void) setCallback:(void (^)(GSListAchievementsResponse*))callback;
@end


@interface GSListChallengeRequest : GSRequest
-(void) setCallback:(void (^)(GSListChallengeResponse*))callback;
-(void) setEntryCount:(NSNumber*) entryCount;
-(void) setOffset:(NSNumber*) offset;
-(void) setShortCode:(NSString*) shortCode;
-(void) setState:(NSString*) state;
-(void) setStates:(NSArray*) states;
@end


@interface GSListChallengeTypeRequest : GSRequest
-(void) setCallback:(void (^)(GSListChallengeTypeResponse*))callback;
@end


@interface GSListGameFriendsRequest : GSRequest
-(void) setCallback:(void (^)(GSListGameFriendsResponse*))callback;
@end


@interface GSListLeaderboardsRequest : GSRequest
-(void) setCallback:(void (^)(GSListLeaderboardsResponse*))callback;
@end


@interface GSListMessageRequest : GSRequest
-(void) setCallback:(void (^)(GSListMessageResponse*))callback;
-(void) setEntryCount:(NSNumber*) entryCount;
-(void) setInclude:(NSString*) include;
-(void) setOffset:(NSNumber*) offset;
@end


@interface GSListMessageSummaryRequest : GSRequest
-(void) setCallback:(void (^)(GSListMessageSummaryResponse*))callback;
-(void) setEntryCount:(NSNumber*) entryCount;
-(void) setOffset:(NSNumber*) offset;
@end


@interface GSListTeamChatRequest : GSRequest
-(void) setCallback:(void (^)(GSListTeamChatResponse*))callback;
-(void) setEntryCount:(NSNumber*) entryCount;
-(void) setOffset:(NSNumber*) offset;
-(void) setOwnerId:(NSString*) ownerId;
-(void) setTeamId:(NSString*) teamId;
-(void) setTeamType:(NSString*) teamType;
@end


@interface GSListVirtualGoodsRequest : GSRequest
-(void) setCallback:(void (^)(GSListVirtualGoodsResponse*))callback;
@end


@interface GSLogChallengeEventRequest : GSRequest
-(void) setCallback:(void (^)(GSLogChallengeEventResponse*))callback;
-(void) setChallengeInstanceId:(NSString*) challengeInstanceId;
-(void) setEventKey:(NSString*) eventKey;
-(void) setEventAttribute:(NSString*)shortCode withNumber:(NSNumber*) value;
-(void) setEventAttribute:(NSString*)shortCode withString:(NSString*) value;
-(void) setEventAttribute:(NSString*)shortCode withDictionary:(NSDictionary*) value;
@end


@interface GSLogEventRequest : GSRequest
-(void) setCallback:(void (^)(GSLogEventResponse*))callback;
-(void) setEventKey:(NSString*) eventKey;
-(void) setEventAttribute:(NSString*)shortCode withNumber:(NSNumber*) value;
-(void) setEventAttribute:(NSString*)shortCode withString:(NSString*) value;
-(void) setEventAttribute:(NSString*)shortCode withDictionary:(NSDictionary*) value;
@end


@interface GSMatchDetailsRequest : GSRequest
-(void) setCallback:(void (^)(GSMatchDetailsResponse*))callback;
-(void) setMatchId:(NSString*) matchId;
@end


@interface GSMatchmakingRequest : GSRequest
-(void) setCallback:(void (^)(GSMatchmakingResponse*))callback;
-(void) setAction:(NSString*) action;
-(void) setMatchGroup:(NSString*) matchGroup;
-(void) setMatchShortCode:(NSString*) matchShortCode;
-(void) setSkill:(NSNumber*) skill;
@end


@interface GSPSNConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setAuthorizationCode:(NSString*) authorizationCode;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setRedirectUri:(NSString*) redirectUri;
-(void) setSegments:(NSDictionary*) segments;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
@end


@interface GSPushRegistrationRequest : GSRequest
-(void) setCallback:(void (^)(GSPushRegistrationResponse*))callback;
-(void) setDeviceOS:(NSString*) deviceOS;
-(void) setPushId:(NSString*) pushId;
@end


@interface GSQQConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setAccessToken:(NSString*) accessToken;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setSegments:(NSDictionary*) segments;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
@end


@interface GSRegistrationRequest : GSRequest
-(void) setCallback:(void (^)(GSRegistrationResponse*))callback;
-(void) setDisplayName:(NSString*) displayName;
-(void) setPassword:(NSString*) password;
-(void) setSegments:(NSDictionary*) segments;
-(void) setUserName:(NSString*) userName;
@end


@interface GSSendFriendMessageRequest : GSRequest
-(void) setCallback:(void (^)(GSSendFriendMessageResponse*))callback;
-(void) setFriendIds:(NSArray*) friendIds;
-(void) setMessage:(NSString*) message;
@end


@interface GSSendTeamChatMessageRequest : GSRequest
-(void) setCallback:(void (^)(GSSendTeamChatMessageResponse*))callback;
-(void) setMessage:(NSString*) message;
-(void) setOwnerId:(NSString*) ownerId;
-(void) setTeamId:(NSString*) teamId;
-(void) setTeamType:(NSString*) teamType;
@end


@interface GSSocialDisconnectRequest : GSRequest
-(void) setCallback:(void (^)(GSSocialDisconnectResponse*))callback;
-(void) setSystemId:(NSString*) systemId;
@end


@interface GSSocialLeaderboardDataRequest : GSRequest
-(void) setCallback:(void (^)(GSLeaderboardDataResponse*))callback;
-(void) setChallengeInstanceId:(NSString*) challengeInstanceId;
-(void) setDontErrorOnNotSocial:(BOOL) dontErrorOnNotSocial;
-(void) setEntryCount:(NSNumber*) entryCount;
-(void) setFriendIds:(NSArray*) friendIds;
-(void) setIncludeFirst:(NSNumber*) includeFirst;
-(void) setIncludeLast:(NSNumber*) includeLast;
-(void) setInverseSocial:(BOOL) inverseSocial;
-(void) setLeaderboardShortCode:(NSString*) leaderboardShortCode;
-(void) setOffset:(NSNumber*) offset;
-(void) setSocial:(BOOL) social;
-(void) setTeamIds:(NSArray*) teamIds;
-(void) setTeamTypes:(NSArray*) teamTypes;
@end


@interface GSSocialStatusRequest : GSRequest
-(void) setCallback:(void (^)(GSSocialStatusResponse*))callback;
@end


@interface GSSteamConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setSegments:(NSDictionary*) segments;
-(void) setSessionTicket:(NSString*) sessionTicket;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
@end


@interface GSTwitchConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setAccessToken:(NSString*) accessToken;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setSegments:(NSDictionary*) segments;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
@end


@interface GSTwitterConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setAccessSecret:(NSString*) accessSecret;
-(void) setAccessToken:(NSString*) accessToken;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setSegments:(NSDictionary*) segments;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
@end


@interface GSViberConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setAccessToken:(NSString*) accessToken;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setSegments:(NSDictionary*) segments;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
@end


@interface GSWeChatConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setAccessToken:(NSString*) accessToken;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setOpenId:(NSString*) openId;
-(void) setSegments:(NSDictionary*) segments;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
@end


@interface GSWindowsBuyGoodsRequest : GSRequest
-(void) setCallback:(void (^)(GSBuyVirtualGoodResponse*))callback;
-(void) setPlatform:(NSString*) platform;
-(void) setReceipt:(NSString*) receipt;
-(void) setUniqueTransactionByPlayer:(BOOL) uniqueTransactionByPlayer;
@end


@interface GSWithdrawChallengeRequest : GSRequest
-(void) setCallback:(void (^)(GSWithdrawChallengeResponse*))callback;
-(void) setChallengeInstanceId:(NSString*) challengeInstanceId;
-(void) setMessage:(NSString*) message;
@end


@interface GSXBOXLiveConnectRequest : GSRequest
-(void) setCallback:(void (^)(GSAuthenticationResponse*))callback;
-(void) setDoNotLinkToCurrentPlayer:(BOOL) doNotLinkToCurrentPlayer;
-(void) setErrorOnSwitch:(BOOL) errorOnSwitch;
-(void) setSegments:(NSDictionary*) segments;
-(void) setStsTokenString:(NSString*) stsTokenString;
-(void) setSwitchIfPossible:(BOOL) switchIfPossible;
-(void) setSyncDisplayName:(BOOL) syncDisplayName;
@end


@interface GSMessageListener : NSObject
	-(void) processMessage:(NSDictionary*) message withType:(NSString*) type;
	-(void) onMessage:(void (^)(GSMessage*))messageListener;
	-(void) onGSAchievementEarnedMessage:(void (^)(GSAchievementEarnedMessage*))GSachievementEarnedMessageListener;
	-(void) onGSChallengeAcceptedMessage:(void (^)(GSChallengeAcceptedMessage*))GSchallengeAcceptedMessageListener;
	-(void) onGSChallengeChangedMessage:(void (^)(GSChallengeChangedMessage*))GSchallengeChangedMessageListener;
	-(void) onGSChallengeChatMessage:(void (^)(GSChallengeChatMessage*))GSchallengeChatMessageListener;
	-(void) onGSChallengeDeclinedMessage:(void (^)(GSChallengeDeclinedMessage*))GSchallengeDeclinedMessageListener;
	-(void) onGSChallengeDrawnMessage:(void (^)(GSChallengeDrawnMessage*))GSchallengeDrawnMessageListener;
	-(void) onGSChallengeExpiredMessage:(void (^)(GSChallengeExpiredMessage*))GSchallengeExpiredMessageListener;
	-(void) onGSChallengeIssuedMessage:(void (^)(GSChallengeIssuedMessage*))GSchallengeIssuedMessageListener;
	-(void) onGSChallengeJoinedMessage:(void (^)(GSChallengeJoinedMessage*))GSchallengeJoinedMessageListener;
	-(void) onGSChallengeLapsedMessage:(void (^)(GSChallengeLapsedMessage*))GSchallengeLapsedMessageListener;
	-(void) onGSChallengeLostMessage:(void (^)(GSChallengeLostMessage*))GSchallengeLostMessageListener;
	-(void) onGSChallengeStartedMessage:(void (^)(GSChallengeStartedMessage*))GSchallengeStartedMessageListener;
	-(void) onGSChallengeTurnTakenMessage:(void (^)(GSChallengeTurnTakenMessage*))GSchallengeTurnTakenMessageListener;
	-(void) onGSChallengeWaitingMessage:(void (^)(GSChallengeWaitingMessage*))GSchallengeWaitingMessageListener;
	-(void) onGSChallengeWithdrawnMessage:(void (^)(GSChallengeWithdrawnMessage*))GSchallengeWithdrawnMessageListener;
	-(void) onGSChallengeWonMessage:(void (^)(GSChallengeWonMessage*))GSchallengeWonMessageListener;
	-(void) onGSFriendMessage:(void (^)(GSFriendMessage*))GSfriendMessageListener;
	-(void) onGSGlobalRankChangedMessage:(void (^)(GSGlobalRankChangedMessage*))GSglobalRankChangedMessageListener;
	-(void) onGSMatchFoundMessage:(void (^)(GSMatchFoundMessage*))GSmatchFoundMessageListener;
	-(void) onGSMatchNotFoundMessage:(void (^)(GSMatchNotFoundMessage*))GSmatchNotFoundMessageListener;
	-(void) onGSMatchUpdatedMessage:(void (^)(GSMatchUpdatedMessage*))GSmatchUpdatedMessageListener;
	-(void) onGSNewHighScoreMessage:(void (^)(GSNewHighScoreMessage*))GSnewHighScoreMessageListener;
	-(void) onGSNewTeamScoreMessage:(void (^)(GSNewTeamScoreMessage*))GSnewTeamScoreMessageListener;
	-(void) onGSScriptMessage:(void (^)(GSScriptMessage*))GSscriptMessageListener;
	-(void) onGSSessionTerminatedMessage:(void (^)(GSSessionTerminatedMessage*))GSsessionTerminatedMessageListener;
	-(void) onGSSocialRankChangedMessage:(void (^)(GSSocialRankChangedMessage*))GSsocialRankChangedMessageListener;
	-(void) onGSTeamChatMessage:(void (^)(GSTeamChatMessage*))GSteamChatMessageListener;
	-(void) onGSTeamRankChangedMessage:(void (^)(GSTeamRankChangedMessage*))GSteamRankChangedMessageListener;
	-(void) onGSUploadCompleteMessage:(void (^)(GSUploadCompleteMessage*))GSuploadCompleteMessageListener;
@end

@interface GSRequestBuilder : NSObject
	+(GSRequest*) buildRequest:(NSDictionary*)data;
@end

#endif

